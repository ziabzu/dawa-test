import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Autocomplete, List, ListItemButton, ListItemText, TextField } from '@mui/material';

function AutocompleteComponent({ sendAddress }) {
  const [query, setQuery] = useState('');
  const [suggestions, setSuggestions] = useState([]);

  useEffect(() => {
    if (query.trim() === '') {
      setSuggestions([]);
      return;
    }

    // Make an API request when the query is changed
    axios
      .get(`https://dawa.aws.dk/adresser/autocomplete?q=${query}`)
      .then((response) => {
        const suggestions = response.data;
        console.log('suggestions')
        console.log(suggestions)
        setSuggestions(suggestions);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [query]);

  const handleChange = (e) => {
    setQuery(e.target.value);
  };

  const clickAddress = (suggestion) => {

    console.log('suggestion');
    console.log(suggestion);

    const address = {
        post_code: suggestion?.adresse?.postnr,
        city: suggestion?.adresse?.postnrnavn,
        lat: suggestion?.adresse?.y,
        long: suggestion?.adresse?.x,
        street: suggestion?.tekst,
    }

    setQuery(suggestion?.tekst)
    sendAddress(address);

  };

  return (
    <div>

    {/* <Autocomplete
      options={suggestions}
      getOptionLabel={(option) => option.tekst} // Adjust this based on your API response structure
      style={{ width: 300 }}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Search"
          variant="outlined"
          onChange={(e) => clickAddress(e.target.value)}
        //   onChange={(e) => () => clickAddress(params)}
        />
      )}
    /> */}

        <TextField id="outlined-basic" label="Outlined" variant="outlined" 
         placeholder="Search for addresses"
         value={query}
         onChange={handleChange}
        />

       <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
            {suggestions.map((suggestion, index) => (
                 <ListItemButton onClick={() => clickAddress(suggestion)}>
                     <ListItemText  key={index} primary={suggestion.tekst} />
                 </ListItemButton>
            ))}
        </List> 

    </div>
  );
}

export default AutocompleteComponent;
